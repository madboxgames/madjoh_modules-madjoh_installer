#!/usr/bin/env node
var argv 		= require('yargs').argv;
var fs 			= require('fs');
var https 		= require('https');
var rimraf 		= require('rimraf');
var prompt 		= require('prompt');
var ncp 		= require('ncp').ncp;
var less 		= require('less');
var requirejs 	= require('requirejs');
var Git 		= require('nodegit');
var config 		= require('./config');

function onErr(err){
	if(err) console.log('Error : ',err);
	return;
}
function parseJSON(string){
	try{
		return JSON.parse(string);
	}catch(e){
		console.log('Parsing Error', string);
		return null;
	}
}

var gitOptions = {
	fetchOpts : {
		callbacks : {
			certificateCheck : function(){return 1;},
			credentials : function (url, userName) {
				return Git.Cred.sshKeyFromAgent(userName);
			}
		}
	}
};

var moduleList 	= [];
var moduleQueue = [];

var settingsList 	= [];
var settingsQueue 	= [];

// lessc --clean-css="--s1 --advanced" main.less main.css

var MadJohInstaller = {
	// path : '../',
	path : './',

	// Regrouping parallel tasks
	ready : function(callback){
		MadJohInstaller.count--;
		if(MadJohInstaller.count === 0) callback();
	},

	// PREPARE FOLDER
		// Make sure that settings folder and madjoh_modules folder exist
		prepareFolders : function(callback){
			if(!fs.existsSync(MadJohInstaller.path + 'dependencies.json')){
				console.log("Your directory is not a MadJoh Project, the dependencies.json file is missing !");
				return;
			}

			MadJohInstaller.count = 3;
			fs.readFile(MadJohInstaller.path + 'dependencies.json', function(err, data){
				if(err) onErr(err);

				MadJohInstaller.dependencies = parseJSON(data);
				MadJohInstaller.ready(callback);
			});

			if(!fs.existsSync(MadJohInstaller.path + 'app/script/settings/')){
				fs.mkdir(MadJohInstaller.path + 'app/script/settings', function(err){
					if(err) return onErr(err);
					MadJohInstaller.settings = {};
					MadJohInstaller.ready(callback);
				});
			}else{
				MadJohInstaller.ready(callback);
			}

			if(!fs.existsSync(MadJohInstaller.path + 'madjoh_modules')){
				fs.mkdir(MadJohInstaller.path + 'madjoh_modules', function(err){
					if(err) return onErr(err);
					MadJohInstaller.modules = {};
					MadJohInstaller.ready(callback);
				});
			}else{
				MadJohInstaller.ready(callback);
			}
		},

		// Tabula rasa mode
		clearModules : function(callback){
			fs.readdir(MadJohInstaller.path + 'madjoh_modules', function(err, data){
				if(err) onErr(err);

				// console.log(data);
				if(data.length === 0){
					callback();
				}else{
					MadJohInstaller.toClear = data.length;
					for(var i=0; i<data.length; i++){
						rimraf(MadJohInstaller.path + 'madjoh_modules/' + data[i], function(err){MadJohInstaller.clearedOne(err, callback);});
					}
				}
			});
		},
		clearedOne : function(err, callback){
			if(err) onErr(err);

			MadJohInstaller.toClear--;
			if(MadJohInstaller.toClear === 0){
				console.log('Cleared the madjoh_modules folder...');
				callback();
			}
		},
		clearBuildFolder : function(callback){
			if(fs.existsSync(MadJohInstaller.path + '../build')){
				rimraf(MadJohInstaller.path + '../build', function(err){
					if(err) return onErr(err);
					callback();
				});
			}else{
				callback();
			}
		},
		copyProject : function(callback){
			var filters = [new RegExp('README.md'), new RegExp('dependencies'), new RegExp('app.js'), new RegExp('styles'), new RegExp('script'), new RegExp('madjoh_modules'), new RegExp('less.min.js'), new RegExp('.git')];

			var options = {
				filter : function(filename){
					var valid = true;
					for(var i = 0; i < filters.length; i++)  valid &= !(filters[i].test(filename));

					return valid;
				}
			}
			ncp('./', '../build', options, function(err){
				if(err) return onErr(err);
				callback();
			});
		},

	// READING AND FINDING DEPENDENCIES
		// Synchronize dependencies and settings
			modules : {},
			settings : {},
			installing : {},
			loadDeps : function(callback){
				MadJohInstaller.count = 2;
				fs.readdir(MadJohInstaller.path + 'madjoh_modules', function(err, data){
					if(err) onErr(err);

					nbModules = 0;
					for(var i=0; i<data.length; i++){
						nbModules += MadJohInstaller.readVersion(MadJohInstaller.path+'madjoh_modules/'+data[i]+'/', 'modules', callback);
					}

					if(nbModules === 0) MadJohInstaller.ready(callback);
					else MadJohInstaller.count--;
					
				});

				fs.readdir(MadJohInstaller.path + 'app/script/settings', function(err, data){
					if(err) onErr(err);

					nbModules = 0;
					for(var i=0; i<data.length; i++){
						nbModules += MadJohInstaller.readVersion(MadJohInstaller.path+'app/script/settings/'+data[i]+'/', 'settings', callback);
					}

					if(nbModules === 0) MadJohInstaller.ready(callback);
					else MadJohInstaller.count--;
				});
			},
			readVersion : function(path, type, callback){
				if(fs.lstatSync(path.substr(0, path.length-1)).isDirectory() && fs.existsSync(path + 'dependencies.json')){
					MadJohInstaller.count ++;
					fs.readFile(path + 'dependencies.json', function(err, data){
						if(err) onErr(err);

						var dependencies = parseJSON(data);
						MadJohInstaller[type][dependencies.name] = dependencies.version;
						MadJohInstaller.ready(callback);
					});

					return 1;
				}else{
					return 0;
				}	
			},
			checkDeps : function(path, module, type, callback){
				fs.readFile(path+'/dependencies.json', function(err, data){
					data = parseJSON(data);
					if(!data) console.log('[WARNING] Corrupted Dependencies File', path + '/dependencies.json');

					if(data.hasStyle){
						if(!MadJohInstaller.dependencies.styles) MadJohInstaller.dependencies.styles = [];
						if(MadJohInstaller.dependencies.styles.indexOf(module) === -1) MadJohInstaller.dependencies.styles.push(module);
					}

						 if(type === 'modules') 	delete MadJohInstaller.installing['madjoh_modules-' 			+ module];
					else if(type === 'settings') 	delete MadJohInstaller.installing['madjoh_modules-settings-' 	+ module];
					MadJohInstaller[type][module] = data.version;

					if(data.dependencies){
						MadJohInstaller.toInstall += Object.keys(data.dependencies).length;
						for(var key in data.dependencies) MadJohInstaller.getTags('madjoh_modules-' + key, data.dependencies[key]);
					}
					if(data.settings){
						MadJohInstaller.toInstall += Object.keys(data.settings).length;
						for(var key in data.settings) MadJohInstaller.getTags('madjoh_modules-settings-' + key, data.settings[key]);
					}
					callback();
				});
			},

			writeDeps : function(callback){
				MadJohInstaller.dependencies.dependencies = {};
				
				var keys = Object.keys(MadJohInstaller.modules);
					keys.sort();

				for(var i = 0; i < keys.length; i++){
					MadJohInstaller.dependencies.dependencies[keys[i]] = MadJohInstaller.modules[keys[i]];
				}

				fs.writeFile(
					MadJohInstaller.path + 'dependencies.json',
					JSON.stringify(MadJohInstaller.dependencies, null, '\t'),
					callback
				);
			},
			writeStyles : function(callback){
				var stylesText = '';
				if(!MadJohInstaller.dependencies.styles) MadJohInstaller.dependencies.styles = [];
				for(var i = 0; i < MadJohInstaller.dependencies.styles.length; i++){
					stylesText += "@import (less) '../../madjoh_modules/" + MadJohInstaller.dependencies.styles[i] + "/" + MadJohInstaller.dependencies.styles[i] + "';\n"
				}
				stylesText += "@import (less) 'general';";

				fs.writeFile(
					MadJohInstaller.path + 'app/styles/main.less',
					stylesText,
					callback
				);
			},

		// Find appropriate version on bitbucket
			enlargeDependencies : function(callback){
				var deps = MadJohInstaller.dependencies.dependencies;
				for(var dep in deps){
					var splitted = deps[dep].split('.');

					if(splitted.length > 1) splitted.pop();
					
					deps[dep] = splitted.join('.');
				}

				callback();
			},
			getTags : function(module, version){
				if(typeof MadJohInstaller.installing[module] !== 'undefined'){
					MadJohInstaller.checkFinish();
				}else{
					https.get('https://bitbucket.org/api/1.0/repositories/madjoh/' + module + '/tags/', function(result){MadJohInstaller.getTagsCallback(result, module, version);}).on('error', onErr);
				}
			},
			getTagsCallback : function(result, module, version){
				MadJohInstaller.installing[module] = "";
				
				result.on('data', function(data){
					MadJohInstaller.installing[module] += data.toString();
				});
				result.on('end', function(){
					var tags = parseJSON(MadJohInstaller.installing[module]);
					if(!tags || tags.error) return console.log('[FATAL] Module not found', module);
					closestVersion = MadJohInstaller.getClosestVersion(tags, version, module);
					if(!closestVersion) console.log('[WARNING] No version defined for this module', module);
					if(closestVersion.indexOf(version) !== 0) console.log(tags);
					MadJohInstaller.get(module, closestVersion);
				});
			},
			parseVersion : function(versionStringArray){
				var version = [];
				for(var i = 0; i < versionStringArray.length; i++) version.push(parseInt(versionStringArray[i]));
				return version;
			},
			getClosestVersion : function(tags, version, module){
				version = version.split('.');
				version = MadJohInstaller.parseVersion(version);
				
				var versions = Object.keys(tags);
				if(versions.length === 0) return false;
				for(var i = 0; i < versions.length; i++) versions[i] = MadJohInstaller.parseVersion(versions[i].substr(1).split('.'));

				var bestIndex;
				for(var j = 0; j < 3; j++){
					var best = -1;
					bestIndex = false;
					for(var i = 0; i < versions.length; i++){
						if(versions[i] === null) continue;
						
						if(j < version.length && versions[i][j] > version[j]) 	versions[i] = null;
						else if(versions[i][j] < best) 							versions[i] = null;
						else if(versions[i][j] > best){
							best = versions[i][j];
							bestIndex = i;
						}
					}
					for(var i = 0; i < versions.length; i++){
						if(versions[i] === null) continue;

						if(versions[i][j] < best) versions[i] = null;
					}
				}
				if(!versions[bestIndex]) return false;

				var closestVersion = versions[bestIndex].join('.');
				return closestVersion;
			},

	// DOWNLOADING FROM BITBUCKET
		get : function(module, version){
			if(module.substr(0, 24) === 'madjoh_modules-settings-') MadJohInstaller.importSettings(module.substr(24), version);
			else MadJohInstaller.install(module.substr(15), version);
		},

		// Install Module
			install : function(module, version){
				if(MadJohInstaller.modules[module] || fs.existsSync(MadJohInstaller.path + 'madjoh_modules/' + module)){
					MadJohInstaller.checkFinish();
				}else{
					var moduleOptions = {module : module, version : version};
					if(moduleList.length < 3){
						moduleList.push(moduleOptions);
						MadJohInstaller.doInstall(module, version);
					}else{
						moduleQueue.push(moduleOptions);
					}
				}
			},
			doInstall : function(module, version){
				var repoUrl 	= 'https://' + config.user_name + '@bitbucket.org/madjoh/madjoh_modules-' + module +'.git';
				var folderPath 	= MadJohInstaller.path + 'madjoh_modules/' + module;
				Git.Clone(repoUrl, folderPath, gitOptions).then(function(repository){
					if(!version) return MadJohInstaller.installCallback(module, version);

					var tagName = 'v' + version;

					repository.getReference(tagName).then(function(reference){
						repository.checkoutRef(reference, {checkoutStrategy : Git.Checkout.STRATEGY.FORCE}).then(function(){
							MadJohInstaller.installCallback(module, version);
						}, function(error){console.log('Git module error :', error);});
					}, function(error){console.log('Git module error :', error);});
				}, function(error){console.log('Git module error :', error);});
			},
			installCallback : function(module, version){
				console.log('Installed module ' + module + ' version : ' + version);
				MadJohInstaller.processQueue(module);
				MadJohInstaller.checkDeps(MadJohInstaller.path + 'madjoh_modules/' + module, module, 'modules', MadJohInstaller.checkFinish);
			},
			processQueue : function(module){
				var index = -1;
				for(var i = 0; i < moduleList.length; i++){
					if(moduleList[i].module === module){
						index = i;
						break;
					}
				}
				if(index > -1) moduleList.splice(index, 1);
				if(moduleQueue.length > 0){
					var nextModule = moduleQueue.shift();
					moduleQueue.push(nextModule);
					MadJohInstaller.doInstall(nextModule.module, nextModule.version);
				}
			},

		// Import Settings
			importSettings : function(settings, version){
				if(MadJohInstaller.settings[settings] || fs.existsSync(MadJohInstaller.path + 'app/script/settings/' + settings)){
					MadJohInstaller.checkFinish();
				}else{
					var settingsOptions = {settings : settings, version : version};
					if(settingsList.length < 3){
						settingsList.push(settingsOptions);
						MadJohInstaller.doInstall(settings, version);
					}else{
						settingsQueue.push(settingsOptions);
					}
				}
			},
			doImportSettings : function(settings, version){
				var repoUrl 	= 'https://' + config.user_name + '@bitbucket.org/madjoh/madjoh_modules-settings-' + settings +'.git';
				var folderPath 	= MadJohInstaller.path + 'app/script/settings/' + settings;
				Git.Clone(repoUrl, folderPath, gitOptions).then(function(repository){
					if(!version) return MadJohInstaller.installCallback(module, version);

					var tagName = 'v' + version;

					repository.getReference(tagName).then(function(reference){
						repository.checkoutRef(reference, {checkoutStrategy : Git.Checkout.STRATEGY.FORCE}).then(function(){
							MadJohInstaller.importSettingsCallback(settings);
						}, function(error){console.log('Git module error :', error);});
					}, function(error){console.log('Git module error :', error);});
				}, function(error){console.log('Git settings error :', error);});
			},
			importSettingsCallback : function(settings){
				rimraf(MadJohInstaller.path + 'app/script/settings/' + settings + '/.git', function(err){
					if(err) return onErr(err);

					console.log('Imported settings ' + settings);
					MadJohInstaller.processSettingsQueue(settings);
					MadJohInstaller.checkDeps(MadJohInstaller.path + 'app/script/settings/' + settings, settings, 'settings', MadJohInstaller.checkFinish);
				});
			},
			processSettingsQueue : function(settings){
				var index = -1;
				for(var i = 0; i < settingsList.length; i++){
					if(settingsList[i].settings === settings){
						index = i;
						break;
					}
				}
				if(index > -1) settingsList.splice(index, 1);
				if(settingsQueue.length > 0){
					var nextSettings = settingsQueue.shift();
					settingsQueue.push(nextSettings);
					MadJohInstaller.doInstall(nextSettings.settings, nextSettings.version);
				}
			},

	// PROCESS END
		checkFinish : function(){
			MadJohInstaller.toInstall--;
			// console.log(MadJohInstaller.toInstall + ' installs left');
			if(MadJohInstaller.toInstall === 0) MadJohInstaller.finishInstall();
		},
		finishInstall : function(){
			MadJohInstaller.count = 2;

			// Update auto-generated files
			MadJohInstaller.writeDeps(function(){MadJohInstaller.ready(MadJohInstaller.finishCallback);});
			MadJohInstaller.writeStyles(function(){MadJohInstaller.ready(MadJohInstaller.finishCallback);});
		},
		finishCallback : function(err){
			if(err) return onErr(err);
			console.log('Installation complete !');
		},

	// BUILDING
		// LESS
			buildLess : function(callback){
				fs.readFile(MadJohInstaller.path + 'app/styles/main.less', function(err, data){
					if(err) return onErr(err);

					data = data.toString();
					less.render(
						data,
						{
							paths: [MadJohInstaller.path + 'app/styles'],
							filename: 'main.less',
							compress: true
						},
						function(err, data){
							if(err) return onErr(err);
							fs.mkdir(MadJohInstaller.path + '../build/app/styles', function(){
								 fs.writeFile(MadJohInstaller.path + '../build/app/styles/main.css', data.css, function(err){
									if(err) return onErr(err);

									callback();
								 })
							});
						}
					);
				});
			},
			rewriteStyles : function(callback){
				fs.readFile(MadJohInstaller.path + '../build/index.html', function(err, data){
					if(err) return onErr(err);

					data = data.toString();

					var reg = new RegExp('<!-- STYLE -->[^]*<!-- /STYLE -->');
					if(!reg.test(data)) console.log('No Styles section found !');
					data = data.replace(reg, '<link rel="stylesheet" type="text/css" href="app/styles/main.css" />');

					fs.writeFile(MadJohInstaller.path + '../build/index.html', data, function(err){
						if(err) onErr(err);
						callback();
					});
				});
			},

		// REQUIRE JS
			buildJS : function(callback){
				var config = {
					baseUrl: MadJohInstaller.path + 'lib',
					paths: {
						app					:  '../app',
						settings			:  '../app/script/settings',
						controllers			:  '../app/script/controllers',
						sub_controllers 	:  '../app/script/sub_controllers',
						displayers 			:  '../app/script/displayers',
						modules 			:  '../app/script/modules',
						apis				:  '../app/script/apis',
						madjoh_modules 		:  '../madjoh_modules',
						module_settings 	: '../app/script/module_settings'
					},
					name: '../app/script/main',
					out: MadJohInstaller.path + '../build/app/script/main.js'
				};

				requirejs.optimize(config, function(){
					fs.readFile(config.out, function(err, data){
						data = data.toString() + '\n' + "require(['../app/script/main'], function(){});";
						fs.writeFile(config.out, data, callback);
					});
				});
			},
			rewriteJS : function(callback){
				fs.readFile(MadJohInstaller.path + '../build/index.html', function(err, data){
					if(err) return onErr(err);

					data = data.toString();

					var scriptText = '';
					if(argv.mobile) scriptText+= '<script type="text/javascript" src="cordova.js"></script>\n\r';
					MadJohInstaller.dependencies.libs = MadJohInstaller.dependencies.libs || [];
					for(var i=0; i<MadJohInstaller.dependencies.libs.length; i++){
						scriptText+= '<script type="text/javascript" src="lib/'+ MadJohInstaller.dependencies.libs[i] +'"></script>\n\r';
					}
					scriptText+= '<script data-main="app/script/main" type="text/javascript" src="lib/require-min.js"></script>\n\r';

					var reg = new RegExp('<!-- SCRIPT -->[^]*<!-- /SCRIPT -->');
					if(!reg.test(data)) console.log('No Script section found !');
					data = data.replace(reg, scriptText);

					fs.writeFile(MadJohInstaller.path + '../build/index.html', data, function(err){
						if(err) onErr(err);
						callback();
					});
				});
			},

	// COMMANDS
		// madjoh --init=type
			init : function(){
				var type = argv.init;
				if(typeof type === 'boolean') type = 'project';
				var name = argv.name;
				if(typeof(name) === 'boolean' || typeof(name) === 'undefined') name = type;
				MadJohInstaller.path += name+'/';

				console.log('Initializing MadJoh '+ type.substr(0,1).toUpperCase()+type.substr(1) +'!');
				
				var repoUrl 	= 'https://' + config.user_name + '@bitbucket.org/madjoh/madjoh_modules-template-' + type +'.git';
				var folderPath 	= MadJohInstaller.path;
				Git.Clone(repoUrl, folderPath, gitOptions).then(
					function(repository){MadJohInstaller.initCallback();},
					function(error){console.log('Git init error :', error);}
				);
			},
			initCallback : function(){
				rimraf(MadJohInstaller.path + '.git', function(err){
					if(err) return onErr(err);

					console.log('Project initialized !');
					if(argv.type !== 'module') MadJohInstaller.prepareFolders(function(){MadJohInstaller.clearModules(MadJohInstaller.installAll);});
				});
			},

		// madjoh --install=module
			installInit : function(){
				MadJohInstaller.prepareFolders(function(){
					MadJohInstaller.loadDeps(function(){
						MadJohInstaller.toInstall = 1;
						MadJohInstaller.install(argv.install, argv.version);
					});
				});
			},

		// madjoh --install
			installAllInit : function(){
				MadJohInstaller.prepareFolders(function(){MadJohInstaller.clearModules(MadJohInstaller.installAll);});
			},
			installAll : function(){
				console.log('Installing all project dependencies');
				var deps = MadJohInstaller.dependencies.dependencies;
				MadJohInstaller.toInstall = Object.keys(deps).length;
				MadJohInstaller.installing = {};
				for(var dep in deps){
					MadJohInstaller.getTags('madjoh_modules-' + dep, deps[dep]);
				}
			},
		
		// madjoh --update
			update : function(){
				console.log('updating all modules');
				MadJohInstaller.prepareFolders(function(){
					MadJohInstaller.clearModules(function(){
						MadJohInstaller.enlargeDependencies(MadJohInstaller.installAll);
					});
				});
			},

		// madjoh --clean
			cleanInit : function(){
				console.log('Which directory would you like to clean ?');
				prompt.start();
				prompt.get(['path'], function (err, result) {
					if(err) return onErr(err);

					var path = result.path;
					if(path.charAt(path.length-1) === ' ') path = path.slice(0, path.length-1);

					MadJohInstaller.clean(path);
				});
			},

			filesToClean : ['README.md', 'dependencies.json'],
			foldersToClean : ['.git'],
			clean : function(path, callback){
				if(!callback) callback = function(){};

				fs.readdir(path, function(err, data){
					if(err) return onErr(err);

					MadJohInstaller.count = data.length * (MadJohInstaller.filesToClean.length + MadJohInstaller.foldersToClean.length);

					for(var i=0; i < data.length; i++){
						// Clean Files
						for(var j=0; j<MadJohInstaller.filesToClean.length; j++){
							if(fs.existsSync(path + '/' + data[i] + '/' + MadJohInstaller.filesToClean[j])){
								fs.unlink(path + '/' + data[i] + '/' + MadJohInstaller.filesToClean[j], function(err){
									if(err) return onErr(err);

									MadJohInstaller.ready(callback);
								});
							}else{
								MadJohInstaller.ready(callback);
							}
						}

						// Clean Folders
						for(var j=0; j<MadJohInstaller.foldersToClean.length; j++){
							if(fs.existsSync(path + '/' + data[i] + '/' + MadJohInstaller.foldersToClean[j])){
								rimraf(path + '/' + data[i] + '/' + MadJohInstaller.foldersToClean[j], function(err){
									if(err) return onErr(err);
									MadJohInstaller.ready(callback);
								});
							}else{
								MadJohInstaller.ready(callback);
							}
						}
					}
				});
			},

		// madjoh --build
			build : function(){
				console.log('Reading project dependencies');
				MadJohInstaller.prepareFolders(function(){

					console.log('Removing existing build folder if exists');
					MadJohInstaller.clearBuildFolder(function(){

						console.log('Duplicating project resources...');
						MadJohInstaller.copyProject(function(){

							console.log('Optimizing RequireJS');
							MadJohInstaller.buildJS(function(){

								console.log('rewriting JavaScript sources');
								MadJohInstaller.rewriteJS(function(){

									if(fs.existsSync(MadJohInstaller.path + 'app/styles/main.less')){
										
										console.log('Building CSS from LESS file');
										MadJohInstaller.buildLess(function(){

											console.log('rewriting Styles sources');
											MadJohInstaller.rewriteStyles(function(){
												console.log('Done');
											});
										});
									}else{
										console.log('No LESS to compile => Done !');
									}
								});
							});
						});
					});
				});
			},

		// madjoh --checkCommits
			checkCommits : function(){
				console.log('Vérification des commit');
				fs.readdir(MadJohInstaller.path + 'madjoh_modules/', function(err, modules) {
					if(err) return console.log(err);
					for(var i = 0; i < modules.length; i++) MadJohInstaller.checkCommit(modules[i]);
				});
			},
			checkCommit : function(module){
				if(module === '.DS_Store') return;

				var path = MadJohInstaller.path + 'madjoh_modules/' + module +'/';
				Git.Repository.open(path).then(function(repository){
					repository.getStatus().then(function(status){
						var statusPicto = (status.length > 0) ? 'x' : 'o';
						if((argv.check_commits === 'verbose' && statusPicto === 'o') || statusPicto === 'x') console.log(statusPicto + ' ' + module);
					}, function(error){return console.log('Error on module status ' + module, error);});
				}, function(error){return console.log('Error on module status ' + module, error);});
			}
};
	
if(argv.init) MadJohInstaller.init();

else if(argv.install){
		 if(typeof argv.install === 'string')	MadJohInstaller.installInit();
	else if(typeof argv.install === 'boolean') 	MadJohInstaller.installAllInit();

}else if(argv.update) MadJohInstaller.update();

else if(argv.test) console.log('No test defined');

else if(argv.clean) MadJohInstaller.cleanInit();

else if(argv.build) MadJohInstaller.build();

else if(argv.check_commits) MadJohInstaller.checkCommits();

else console.log('Command not found');